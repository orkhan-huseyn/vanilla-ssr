const React = require('react');

function Home() {
  const [count, setCount] = React.useState(0);

  return React.createElement(
    'div',
    null,
    React.createElement('h1', null, 'Hello, World!'),
    React.createElement('p', null, 'The count is ', count),
    React.createElement(
      'button',
      {
        onClick: () => setCount(count + 1),
      },
      'Increment'
    )
  );
}

module.exports = Home;
