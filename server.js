const express = require('express');
const path = require('path');
const { renderToString } = require('react-dom/server');
const React = require('react');
const Home = require('./components/Home');

const app = express();

app.use('/assets', express.static(path.resolve('assets')));

app.get('/', (req, res) => {
  const content = renderToString(React.createElement(Home));
  res.send(`
        <!DOCTYPE html>
        <html>
            <head>
                <title>SSR App</title>
            </head>
            <body>
                <div id="root">${content}</div>
                <script defer src="assets/bundle.js"></script>
            </body>
        </html>
    `);
});

app.listen(8080, () => {
  console.log('Server is running on port 8080');
});
