const React = require('react');
const ReactDOM = require('react-dom/client');
const Home = require('./components/Home');

const container = document.getElementById('root');
ReactDOM.hydrateRoot(container, React.createElement(Home));
